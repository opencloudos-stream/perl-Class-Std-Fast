Name:           perl-Class-Std-Fast
Version:        0.0.8
Release:    1%{?dist}
Summary:        Faster but less secure replacement for Class::Std
License:        GPL+ or Artistic
URL:            https://metacpan.org/release/Class-Std-Fast
Source0:        https://cpan.metacpan.org/authors/id/A/AC/ACID/Class-Std-Fast-v%{version}.tar.gz
Source1:        http://dev.perl.org/licenses/#/%{name}-Licensing.html

BuildArch:      noarch

BuildRequires: make
BuildRequires:  perl-generators
BuildRequires:  perl-interpreter
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(base)
BuildRequires:  perl(Carp)
BuildRequires:  perl(Class::Std)
BuildRequires:  perl(Data::Dumper)
BuildRequires:  perl(English)
BuildRequires:  perl(lib)
BuildRequires:  perl(Storable)
BuildRequires:  perl(strict)
BuildRequires:  perl(warnings)
BuildRequires:  perl(Test::More)
BuildRequires:  perl(Test::Pod)
BuildRequires:  perl(Test::Pod::Coverage)
BuildRequires:  perl(version)
BuildRequires:  perl(warnings)

Requires:       perl(Class::Std)
Requires:       perl(Data::Dumper)


%description
Class::Std::Fast allows you to use the beautiful API of Class::Std in a faster
way than Class::Std does. You can get the object's identity via scalar-ifying 
our object. Getting the objects identity is still possible via the ident method.


%prep
%setup -q -n Class-Std-Fast-v%{version}
cp -a %{SOURCE1} Licensing.html

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 NO_PERLLOCAL=1
%{make_build}


%install
%{make_install}
%{_fixperms} %{buildroot}


%check
make test


%files
%license Licensing.html
%doc Changes README
%{perl_vendorlib}/Class/*
%{_mandir}/man3/*.3*


%changelog
* Fri Apr 12 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.0.8-1
- initial build
